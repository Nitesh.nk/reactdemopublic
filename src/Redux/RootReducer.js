import { combineReducers } from "redux";
import CommonReducer from "../Redux/CommonReducer";
import LoginReducer from "../components/Login/LoginReducer";
// Combine all reducers.

const rootReducer = (state, action) => {
  // Clear all data in redux store to initial.

  if (action.type === "Logout") state = undefined;

  return appReducer(state, action);
};

const appReducer = combineReducers({
  CommonReducer,

  LoginReducer,

});

export default rootReducer;
