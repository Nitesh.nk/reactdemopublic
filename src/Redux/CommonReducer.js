import {
  ACCESS_RIGHTS_LOADING,
  ACCESS_RIGHTS_SUCCESS,
  ACCESS_RIGHTS_FAILURE
} from "../Redux/Types";

const initialState = {
  loginError: "",
  accessRightsLoading: false,
  accessRightsData: []
};

const CommonReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACCESS_RIGHTS_LOADING:
      return {
        ...state,
        accessRightsLoading: true
      };

    case ACCESS_RIGHTS_FAILURE:
      return {
        ...state,
        loginError: action.payload.data,
        accessRightsLoading: false
      };

    case ACCESS_RIGHTS_SUCCESS:
      return {
        ...state,
        loginError: "",
        accessRightsLoading: false
      };
    default:
      return state;
  }
};

export default CommonReducer;
