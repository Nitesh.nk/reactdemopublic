//Login
export const LOGIN_LOADING = "Login_Loading";
export const LOGIN_SUCCESS = "Login_Success";
export const LOGIN_FAILURE = "Login_Failure";
export const LOGIN = "Login";
export const ACCESS_RIGHTS_LOADING = "ACCESS_RIGHTS_LOADING";
export const ACCESS_RIGHTS_SUCCESS = "ACCESS_RIGHTS_SUCCESS";
export const ACCESS_RIGHTS_FAILURE = "ACCESS_RIGHTS_FAILURE";
