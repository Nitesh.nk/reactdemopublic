import { apiCall as api } from "../Networking/withAPI";
export const callApi = (action, method, body, isSuccess, isFail) => async (
  dispatch
) => {
  //TODO: check internet here
  //TODO: put types in constants that being appended after action
  dispatch({ type: action + "_Loading" });
  let { apiSuccess, data } = await api(method, body);
  if (apiSuccess) {
    await dispatch({
      type: action + "_Success",
      payload: { data }
    });
    isSuccess && isSuccess(data);
  } else {
    await dispatch({
      type: action + "_Failure",
      payload: { data }
    });
    isFail && isFail(data);
  }
};
