export const isVaild = (type, val) => {//TODO: Tirath task
    let re;
    switch (type) {
        case 'mobilenum':
            re = /^[0][1-9]\d{9}$|^[1-9]\d{9}$/;
            break;
        case 'pincode':
            re = /^[1-9][0-9]{5}$/;
            break;
        case 'Otp':
            re = /^\d{4}[1-9]*$/;
            break;
        case 'text':
            re = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
            break;
        case 'int':
            re = /^\d*[1-9]\d*$/;
            break;
        case 'only1num':
            re = /^([1-9]|1[0])$/;
            break;
        case 'notspace':
            re = /^(\w+\S+)$/;
            break;
        case 'email':
            re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            break;
        default:
            re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            break;
    }
    return val && re.test(val) && val.trim();
}
