/* eslint-disable camelcase */
import { GetBaseURL } from "../Networking/Urls";
import Axios from "axios";

let axiosInstance;
const connectionTimeout = 20000; // 20 Secs

async function getAxiosInstance() {
  const accessToken = localStorage.getItem("nitesh");
  axiosInstance = Axios.create({
    baseURL: GetBaseURL(),
    timeout: connectionTimeout,
    headers: {
      "access-token": accessToken,
      "module": "User"
    }
  });
  return axiosInstance;

}

export const apiCall = async (request, payload) => {
  let { URL } = request;
  const axiosInstance = await getAxiosInstance();
  try {
    const response = await axiosInstance.post(URL, payload);
    if (response.status === 200) {
      if (response.data.status === 200) {
        return { apiSuccess: true, data: response.data };
      } else {
        return { apiSuccess: false, data: response.data };
      }
    } else {
      return { apiSuccess: false, data: response };
    }
  } catch (error) {
    return {
      data: error?.response?.data || error,
      apiSuccess: false
    };
  }
};
