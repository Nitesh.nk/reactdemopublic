export function GetBaseURL() {
  return "http://3.6.108.152:4300/";
  // return "http://192.168.43.219:3016/";
}

export const URLs = {
  LOGIN_USER: {
    URL: "login"
  },
  BEAT_STORE_LIST: {
    URL: "tra/trans/beatStoreList"
  },
  FORGOT_PWD: {
    URL: "tra/usr/forgotPin"
  },
  BEATS: {
    URL: "tra/trans/beatList"
  },
  DAY_START: {
    URL: "tra/trans/dayStart"
  },
  COMMON_DATA: {
    URL: "tra/trans/pltCommonData"
  },
  VALIDATE_OTP: {
    URL: "tra/usr/validateotp"
  },
  BEAT_STORE_VISITED_START: {
    URL: "tra/trans/beatStoreVisitStart"
  },
  STORE_VISIT_CANCEL: {
    URL: "tra/trans/beatStoreVisitCancel"
  },
  STORE_VISIT_FINISH: {
    URL: "tra/trans/beatStoreVisitFinish"
  },
  CATEGORY: {
    URL: "tra/mch/cat"
  },
  SUB_CATEGORY: {
    URL: "tra/mch/subcat"
  },
  SUB_CATEGORY_CHILD: {
    URL: "tra/mch/subchildcat"
  },
  CATEGORY_CHILD: {
    URL: "tra/mch/childcat"
  },
  TIME_LINE: {
    URL: "tra/ana/timeLine"
  },
  END_DAY: {
    URL: "tra/trans/dayEnd"
  },
  SIGN_UP: {
    URL: "tra/ana/signup"
  },
  BANK_DETAIL: { URL: "tra/usr/bankDetails" },
  SIGN_UP2: {
    URL: "tra/usr/signup2"
  },
  DOCUMENT_UPLOAD: {
    URL: "tra/usr/signup3"
  },
  ADDRESS: {
    URL: "tra/usr/addAdress"
  },
  ONBOARDING_OTP: {
    URL: "tra/usr/validatemobile"
  },
  ONBOARDING_OTP_RESEND: {
    URL: "tra/usr/forgotpwdusr"
  },
  USER_PROCESS: {
    URL: "tra/trans/userinprocess"
  },
  USER_DETAILS: {
    URL: "tra/trans/userDtl"
  },
  ALL_PRODUCT: {
    URL: "tra/mch/productList"
  },
  PRODUCT_DETAIL: {
    URL: "tra/mch/productinfo"
  }
};
