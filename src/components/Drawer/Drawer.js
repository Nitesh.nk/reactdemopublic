import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { ListItem, withStyles } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        // width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },

    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    }
}));

const StyledListItem = withStyles({
    root: {
        "&$selected": {
            backgroundColor: "red"
        }
    },
    selected: {}
})(ListItem);

export default function MiniDrawer() {
    const classes = useStyles();


    const handleClicked = () => {
        console.log("firstList Clicked")
    }

    return (
        <div className={classes.root}>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerClose]: true,
                    paper: {}
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerClose]: true
                    }),
                }}
            >
                <List>
                    {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                        <StyledListItem button onClick={handleClicked} selected={index == 1 ? true : false} key={text}>
                            <ListItemIcon> <MailIcon /></ListItemIcon>
                            <ListItemText primary={text} />
                        </StyledListItem>
                    ))}
                </List>
            </Drawer>

        </div>
    );
}
