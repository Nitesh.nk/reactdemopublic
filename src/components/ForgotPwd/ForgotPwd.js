import React, { useState } from 'react';
import { Input, Button } from '../../Common/Common'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux';
import { callApi } from "../../Redux/CommonDispatch";
import { LOGIN } from "../../Redux/Types";
import { URLs } from "../../Networking/Urls";
import CircularProgress from '@material-ui/core/CircularProgress';
import { withSnackbar } from 'notistack';

function ForgotPwd(props) {
    const [userDetails, setUserDetails] = useState({ username_mobile: '' });
    const state = useSelector(state => state.store);
    const dispatch = useDispatch()

    let isSuccess = (message) => {
        props.enqueueSnackbar(message, {
            variant: 'success', anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
            },
        })
    };

    let isFail = (message) => {
        props.enqueueSnackbar(message, {
            variant: 'error', anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
            },
        })
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (userDetails.username_mobile)
            dispatch(callApi(LOGIN, URLs.LOGIN_USER, userDetails, isSuccess, isFail));
    }

    return (
        <>
            <form onSubmit={(e) => onSubmit(e)}>
                <Input type="text" id="username_mobile" label="Username" value={userDetails.username_mobile} required={true} onChange={(e) => setUserDetails({ username_mobile: e.target.value })} autoFocus={true} />
                {state.loader ? <CircularProgress /> : <Button title="Submit" />}

            </form>

        </>
    );
}

export default withSnackbar(ForgotPwd);
