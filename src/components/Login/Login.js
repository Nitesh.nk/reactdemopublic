import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withSnackbar } from 'notistack';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MInput } from '../../Common/Common';
import { URLs } from "../../Networking/Urls";
import { callApi } from "../../Redux/CommonDispatch";
import { LOGIN } from "../../Redux/Types";
import './Login.css';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';


function Login(props) {
    const [loginDetails, setLoginDetails] = useState({ usernameEmail: '', password: '' });
    const [errormsg, setErrorMsg] = useState({ usernameEmailErr: '', passwordErr: '' });
    const [collapsed, setCollapsed] = useState(true);

    const toggleNavbar = () => setCollapsed(!collapsed);

    const state = useSelector(state => state.LoginReducer);
    const dispatch = useDispatch()


    let isSuccess = ({ message, data }) => {
        if (false) {
            sessionStorage.setItem("token", data.token);
        } else {
            localStorage.setItem("token", data.token);
        }
        props.enqueueSnackbar(message, {
            variant: 'success', anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
            },
        })
    };

    let isFail = ({ message }) => {
        props.enqueueSnackbar(message, {
            variant: 'error', anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
            },
        })
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (loginDetails.usernameEmail && loginDetails.password)
            dispatch(callApi(LOGIN, URLs.LOGIN_USER, loginDetails, isSuccess, isFail));
        else if (!loginDetails.usernameEmail)
            setErrorMsg({ usernameEmailErr: 'Name is required' })
        else if (!loginDetails.password)
            setErrorMsg({ passwordErr: 'Password is required' })
    }
    const nameChange = (e) => {
        setLoginDetails({ usernameEmail: e.target.value, password: loginDetails.password });
        setErrorMsg({ usernameEmailErr: '', passwordErr: errormsg.passwordErr })
    }

    const passChange = (e) => {
        setLoginDetails({ usernameEmail: loginDetails.usernameEmail, password: e.target.value });
        setErrorMsg({ usernameEmailErr: errormsg.usernameEmailErr, passwordErr: '' })
    }
    return (
        <>
            {/* <Navbar light style={{ backgroundColor: 'skyblue' }}>
                <NavbarBrand></NavbarBrand>
                <NavbarToggler onClick={toggleNavbar} />
                <Collapse isOpen={!collapsed} navbar>
                    <Nav navbar>
                        <NavItem>
                            <NavLink href="/components/">Components</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar> */}
            {/* <div class="d-flex p-3 bg-secondary text-white">
                <div class="p-2 bg-info">Flex item 1</div>
                <div class="p-2 bg-warning">Flex item 2</div>
                <div class="p-2 bg-primary">Flex item 3</div>
            </div> */}
            <div class="d-flex flex-row-reverse">
                <div class="flex-fill bg-info">Flex item 1</div>
                <div class="flex-fill bg-warning">Flex item 2</div>
                <div class="flex-fill bg-primary">Flex item 3</div>
            </div>
            <div className="login-div">
                <div className="login-box">
                    <form onSubmit={(e) => onSubmit(e)}>
                        <br />
                        <MInput
                            error={errormsg.usernameEmailErr || ''}
                            label="Username"
                            helperText={errormsg.usernameEmailErr}
                            value={loginDetails.usernameEmail}
                            onChange={(e) => nameChange(e)}
                            autoFocus={true} />
                        <br />
                        <br />
                        <MInput
                            error={errormsg.passwordErr || ''}
                            label="Password"
                            helperText={errormsg.passwordErr}
                            value={loginDetails.password}
                            onChange={(e) => passChange(e)} />
                        <br />
                        <br />
                        {state.loginLoading ?
                            <CircularProgress /> :
                            <Button type="Submit" variant="contained" color="primary">
                                Primary
                    </Button>
                        }
                        <br />

                        {/* <Button variant="contained" disabled>
                    Disabled
                </Button> */}

                    </form>
                </div>
            </div>
        </>
    );
}

export default withSnackbar(Login);
