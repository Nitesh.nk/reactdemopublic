import { LOGIN_FAILURE, LOGIN_LOADING, LOGIN_SUCCESS } from "../../Redux/Types";

const initialState = {
  loginError: null,
  loginLoading: false,
  loginErrorStatus: null,
};

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_LOADING:
      return {
        ...state,
        loginLoading: true
      };

    case LOGIN_FAILURE:
      return {
        ...state,
        loginError: action.payload?.data?.message,
        loginLoading: false,
        loginErrorStatus: action.payload?.data?.status
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        loginError: null,
        loginLoading: false,
        loginErrorStatus: null
      };
    default:
      return state;
  }
};

export default LoginReducer;
