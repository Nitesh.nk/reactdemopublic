import React, { Component } from 'react';
import { isVaild } from '../Validator/Validator';
import TextField from '@material-ui/core/TextField';

import './Common.css';

export function Button(props) {
    let cName = props.cName ? "accountpe-btn " + props.cName : "accountpe-btn";
    return (
        <button id={props.id} className={cName} onClick={props.type === "submit" ? null : props.onClick} tabIndex={props.tabIndex}>{props.children ? props.children : props.title}</button>
    )
}


export function MInput(props) {
    const { error, helperText, label, autoFocus, onChange } = props
    return (
        <TextField
            error={error}
            // id="outlined-basic"  //TODO:Need to pass unique id from props
            label={label}
            helperText={helperText}
            variant="outlined"
            autoFocus={autoFocus}
            onChange={(e) => onChange(e)}
        />)
}
export class Input extends Component {
    state = {
        errMsg: '',
        hasErr: false
    }
    hasRequiredError() {
        return this.props.showError && this.props.required && !(this.props.value && (typeof (this.props.value) === 'number' || this.props.value.trim()));
    }

    getClass() {
        let cName = this.props.cName ? "accountpe-input " + this.props.cName : "accountpe-input";
        cName = this.hasRequiredError() ? cName + " accountpe-input-error" : cName;
        cName = this.props.readonly ? cName + " readonly" : cName;
        return cName;
    }

    onChange(e) {
        let re, errMsg, sync = false;
        switch (this.props.iType) {
            case 'num':
                re = /^[0-9\b]+$/;
                break;
            case 'email':
                re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                errMsg = 'Please enter valid email id';
                sync = true;
                break;
            case 'mobilenum':
                re = /^[0][1-9]\d{9}$|^[1-9]\d{9}$/;
                errMsg = 'Please enter 10 digit number';
                sync = true;
                break;
            case 'Otp':
                re = /^\d{4}[1-9]*$/;
                errMsg = 'Please enter valid otp';
                sync = true;
                break;
            case 'pincode':
                re = /^[1-9][0-9]{5}$/;
                errMsg = 'Please enter 6 digit pincode';
                sync = true;
                break;
            case 'int':
                re = /^\d*[1-9]\d*$/;
                errMsg = 'Please enter a number'
                sync = true;
                break;
            case 'only1num':
                re = /^([1-9]|1[0])$/;
                errMsg = 'Please enter only one digit';
                sync = true;
                break;
            case 'textfiled':
                re = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
                errMsg = 'Please enter your name in charater';
                sync = true;
                break;
            case 'notspace':
                re = /^(\w+\S+)$/;
                errMsg = 'Please enter valid input';
                sync = true;
                break;
            default:
                break;
        }
        if (!re.test(e.target.value)) {
            this.setState({ errMsg: errMsg, hasErr: true })
        } else {
            this.setState({ errMsg: '', hasErr: false })
            this.props.onChange(e);
        }
        if (sync || !e.target.value) {
            this.props.onChange(e);
        }
    }

    render() {
        if (this.props.value && this.props.iType && this.state.hasErr && isVaild(this.props.iType, this.props.value)) {
            this.setState({ errMsg: '', hasErr: false })
        }
        return (
            <>
                {this.props.label && !this.props.noLabel && (
                    <Label title={this.props.label + (this.props.required ? "*" : "")} cName={"accountpe-input-label " + this.props.colName} />
                )}
                <input type={this.props.type} id={this.props.id} list={this.props.list} className={this.getClass()} placeholder={this.props.placeholder} value={this.props.value} onChange={(e) => this.props.iType ? this.onChange(e) : this.props.onChange(e)} tabIndex={this.props.tabIndex} onFocus={(e) => this.props.onFocus ? this.props.onFocus(e, true) : null} onBlur={(e) => this.props.onFocus ? this.props.onFocus(e) : null} autoFocus={this.props.autoFocus} accept={this.props.accept} />
                {!this.state.hasErr && this.hasRequiredError() && (
                    <Label cName={"q-manager-label-error " + this.props.colNameError} title={this.props.label + " is required"} />
                )}
                {this.props.iType && this.state.hasErr && this.state.errMsg && !this.props.noLabel && (
                    <Label cName={"q-manager-label-error " + this.props.colNameError} title={this.state.errMsg} />
                )}
            </>
        )
    }
}


export function Label(props) {
    return (
        <label className={props.cName ? "q-manager-label " + props.cName : "q-manager-label"} onClick={props.onClick ? props.onClick : null}>{props.children ? props.children : props.title}</label>
    )
}
