import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom'
import Login from './components/Login/Login'
import ForgotPwd from './components/ForgotPwd/ForgotPwd'
import Drawer from './components/Drawer/Drawer'
import Header from './components/Header/Header'
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/forgotpwd" component={ForgotPwd} title="Log In" />
          <Route path="/login" component={Login} title="Log In" />
          <Route path="/" component={Drawer} title="Log In" />
          <Redirect from='*' to='/' />
        </Switch>
      </div>
    );
  }
}

export default App;
